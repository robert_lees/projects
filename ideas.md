# Ideas

Style inspired by https://brutask.com/

Projects have Tasks

A task is a "todo", tasks can have "blockers" which change state of task to "not my problem" / "awaiting info"

Homepage shows all tasks, or by project

There can be an overall "stats" management view later

Projects have users, used for stats/mass assigning page. I.e see everyones work/modify reorder etc