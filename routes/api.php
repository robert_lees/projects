<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('users/{user}/tasks', [\App\Http\Controllers\Api\TaskController::class, 'userTasks']);
Route::get('users/{user}/incomplete-tasks', [\App\Http\Controllers\Api\TaskController::class, 'userIncompleteTasks']);
Route::get('users/{user}/complete-tasks', [\App\Http\Controllers\Api\TaskController::class, 'userCompleteTasks']);

Route::post('tasks/{task}/complete', [\App\Http\Controllers\Api\TaskController::class, 'complete']);
Route::post('tasks/{task}/uncomplete', [\App\Http\Controllers\Api\TaskController::class, 'uncomplete']);

Route::post('tasks', [\App\Http\Controllers\Api\TaskController::class, 'store']);
