import './bootstrap';
import mitt from 'mitt';
import { createApp } from 'vue';
import TaskList from './components/TaskList.vue';
import CreateTaskButton from './components/CreateTaskButton.vue';

const emitter = mitt();

const app = createApp({
    name: "Tasks",
    components: {
        TaskList,
        CreateTaskButton
    },
});

app.config.globalProperties.emitter = emitter;
app.mount('#app');