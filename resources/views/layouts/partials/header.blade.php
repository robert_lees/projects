<header class="py-4 bg-header mb-4">
    <div class="container mx-auto">
        <form method="POST" action="{{ route('logout') }}" ref="logoutForm">
            @csrf

<a href="{{route('logout') }}" @click.prevent="$refs.logoutForm.submit();" class="text-primary">
                {{ __('Logout') }}
            </a>
        </form>
    </div>

</header>