<x-app-layout>

    <div class="my-12">
<h1 class="text-3xl font-extrabold my-6 text-primary">Today's Tasks</h1>
        <task-list></task-list>
<create-task-button></create-task-button>
    </div>




</x-app-layout>