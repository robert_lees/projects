<?php

namespace App\Http\Controllers\Api;

use App\Models\Task;
use App\Models\User;
use App\Models\Status;
use App\Enums\StatusType;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task($request->all());
        $task->status()->associate(Status::wherePending()->first());
        $task->save();
        $task->users()->attach(User::find(1));
        return $task;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function complete(Task $task)
    {
        if ($task->status->status_type == StatusType::COMPLETED) {
            abort(400, "Task already marked completed.");
        }
        $task->completed_at = now();
        $task->completedBy()->associate(auth()->user());
        $task->status()->associate(Status::whereCompleted()->first());
        $task->save();
        return $task;
    }

    public function uncomplete(Task $task)
    {
        if ($task->status->status_type !== StatusType::COMPLETED) {
            abort(400, "Task already marked not completed.");
        }
        $task->completed_at = null;
        $task->completedBy()->associate(null);
        $task->save();
        return $task;
    }

    public function userTasks(User $user){
        return $user->tasks;
    }

    public function userIncompleteTasks(User $user)
    {
        return $user->tasks;
    }

    public function userCompleteTasks(User $user)
    {
        return $user->tasks;
    }
}
