<?php

namespace App\Models;

use App\Enums\StatusType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Status extends Model
{
    use HasFactory;

    public static function defaultPendingStatus()
    {
        return static::wherePending()->first();
    }

    public function scopeWherePending($query)
    {
        return $query->where('status_type', StatusType::PENDING);
    }

    public function scopeWhereBlocked($query)
    {
        return $query->where('status_type', StatusType::BLOCKED);
    }

    public function scopeWhereCompleted($query)
    {
        return $query->where('status_type', StatusType::COMPLETED);
    }

    public function scopeWhereCancelled($query)
    {
        return $query->where('status_type', StatusType::CANCELLED);
    }

    public function scopeWhereInProgress($query)
    {
        return $query->whereNotIn('status_type', [StatusType::COMPLETED, StatusType::CANCELLED]);
    }
}
