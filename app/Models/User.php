<?php

namespace App\Models;

use App\Enums\StatusType;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tasks()
    {
        return $this->belongsToMany(Task::class)->orderBy('importance', 'desc')->orderBy('due_date', 'asc')->orderBy('created_at', 'asc');
    }

    public function incompleteTasks()
    {
        return $this->tasks()->whereHas('status', function ($q) {
            $q->status_type !== StatusType::COMPLETED;
        });
    }

    public function completeTasks()
    {
        return $this->tasks()->whereHas('status', function ($q) {
            $q->status_type === StatusType::COMPLETED;
        });
    }
}
