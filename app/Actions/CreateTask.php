<?php
namespace App\Actions;

use App\Models\Task;
use App\Models\Status;

class CreateTask
{
    public function execute(array $data)
    {
        $task = new Task($data);
        $task->status()->associate(Status::defaultPendingStatus());
        $task->save();
        return $task;
    }
}
