<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

/** @psalm-immutable */
class StatusType extends Enum
{
    public const PENDING = 'pending';
    public const BLOCKED = 'blocked';
    public const COMPLETED = 'completed';
    public const IN_PROGRESS = 'in_progress';
    public const CANCELLED = 'cancelled';

    private static array $labelMap = [
        self::PENDING => 'Pending',
        self::BLOCKED => 'Blocked',
        self::IN_PROGRESS => 'In Progress',
        self::COMPLETED => 'Completed',
        self::CANCELLED => 'Cancelled',
    ];

    public static function getLabels(): array
    {
        return self::$labelMap;
    }
}
