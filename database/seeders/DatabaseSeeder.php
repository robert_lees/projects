<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        collect([
            new UsersTableSeeder(),
            new StatusesTableSeeder(),
            new TasksTableSeeder()
        ])->each->run();
    }
}
