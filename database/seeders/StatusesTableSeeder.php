<?php

namespace Database\Seeders;

use App\Models\Status;
use App\Enums\StatusType;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create(['title' => 'Pending', 'status_type' => StatusType::PENDING]);
        Status::create(['title' => 'In progress', 'status_type' => StatusType::IN_PROGRESS]);
        Status::create(['title' => 'Completed', 'status_type' => StatusType::COMPLETED]);
        Status::create(['title' => 'Awaiting Information', 'status_type' => StatusType::BLOCKED]);
        Status::create(['title' => 'Cancelled', 'status_type' => StatusType::CANCELLED]);
    }
}
