<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create(['email' => 'rob@senses.co.uk', 'password' => '$2y$10$R5erC503yQnM3KSLUi89aeBeX1ABsGE/HYET13KvNXTtn7/1I36Ya']);
        User::factory(10)->create();
    }
}
