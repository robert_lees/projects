<?php

namespace Database\Factories;

use App\Models\Status;
use App\Enums\StatusType;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Status::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text,
            'status_type' => $this->faker->randomElement(array_keys(StatusType::getLabels()))
        ];
    }
}
