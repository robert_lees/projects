const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors')

module.exports = {
    mode: 'jit',
    purge: ['./resources/**/*.{js,vue,blade.php}'],

    theme: {

        extend: {
            fontFamily: {
                sans: ['Inter', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                gray: colors.gray,
                primary: "var(--primary)",
                secondary: "var(--secondary)",
                main: "var(--main)",
                background: "var(--background)",
                header: "var(--header)",
                accent: "var(--accent)",
            },
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
